# -*- coding: utf-8 -*-
import re
import scrapy

from scrapy.http import Request, HtmlResponse
from scrapy.linkextractors import LinkExtractor

from urllib.parse import urlparse


class SitemapextractorSpider(scrapy.Spider):
    name = 'sitemapextractor'

    def __init__(self, **kw):
        """
        Redefining the default __init__ method in order to provide
        url from spider parameters and also to override allowed_domains
        """
        super(SitemapextractorSpider, self).__init__(**kw)
        self.url = kw.get('url')
        self.link_extractor = LinkExtractor()
        self.allowed_domains = [
            re.sub(r'^www\.', '', urlparse(self.url).hostname)]

    def start_requests(self):
        return [Request(self.url, callback=self.parse)]

    def parse(self, response):
        # Extracting all links from current page, and following them
        if isinstance(response, HtmlResponse):
            next_links = self.link_extractor.extract_links(response)
            for next_link in next_links:
                yield response.follow(next_link, self.parse)
        # Extracting items itself
        yield {'url': response.url}
