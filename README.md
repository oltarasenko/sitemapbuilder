# Sitemap Builder

The sitemap builder is a Scrapy project which helps to extract all urls from a given domain
and to store them in multiple formats

# Installing and running the crawler

Use python virtualenv in order to install and run the project. The required steps are following:

1. `pip install -r requirements.txt` installs all the requirements (Scrapy in our case)
2. `scrapy crawl sitemapextractor -a url="https://scrapy.org/" -o links.csv` will run the crawler against the scrapy.org website and will store links in links.csv file

# Configuration

The crawler can be configured via the `settings.py` file.
Currently we're limiting the number of scraped links at 100, but this can be tuned using `CLOSESPIDER_ITEMCOUNT` setting.

# Limitations

Current application is capable for building sitemaps for any non Javascript vebsites (here we mean JS navigation) without massive untibot protection.

Current application is following robots.txt rules found on the target websites 

# Sample items

Samples of scraped links can be found in ./items.csv file
